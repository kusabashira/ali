package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
	"text/tabwriter"
)

var (
	commandName = "ali"
	version     = "v1.0.0"
)

func printVersion() {
	fmt.Fprintf(os.Stderr, "%s: %s\n",
		commandName, version)
}

func printUsage() {
	fmt.Fprintf(os.Stderr, `
Write aligned concatenation of all FILE(s) to standard output.
Usage: %s [OPTION] [FILE]...

Options:
	--help        show this help message
	--version     print the version

Report bugs to <kusabashira227@gmail.com>
`[1:], commandName)
}

var spaces = regexp.MustCompile(`\s+`)

func toTabSeparetedLine(line string) string {
	spLine := spaces.Split(line, -1)
	return strings.Join(spLine, "\t")
}

func _main() error {
	var (
		isHelp    = flag.Bool("help", false, "show this help message")
		isVersion = flag.Bool("version", false, "print the version")
	)
	flag.Usage = printUsage
	flag.Parse()
	if *isHelp {
		printUsage()
		return nil
	}
	if *isVersion {
		printVersion()
		return nil
	}

	var input io.Reader
	if flag.NArg() < 1 {
		input = os.Stdin
	} else {
		var inputs []io.Reader
		for _, fname := range os.Args {
			inFile, err := os.Open(fname)
			if err != nil {
				return err
			}
			defer inFile.Close()
			inputs = append(inputs, inFile)
		}
		input = io.MultiReader(inputs...)
	}

	writer := new(tabwriter.Writer)
	writer.Init(os.Stdout, 0, 8, 1, ' ', 0)
	reader := bufio.NewScanner(input)
	for reader.Scan() {
		fmt.Fprint(writer, toTabSeparetedLine(reader.Text()))
		fmt.Fprint(writer, "\t\n")
	}
	writer.Flush()

	return nil
}

func main() {
	err := _main()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %v", commandName, err)
		os.Exit(1)
	}
}
